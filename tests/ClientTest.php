<?php

namespace Zpg\Tests;

use Zpg\Model\Branch;
use Zpg\Model\BranchLocation;
use Zpg\Model\Update;
use Zpg\Model\UpdateDetailedDescriptionItem;
use Zpg\Model\UpdateLocation;
use Zpg\Model\UpdatePricing;

class ClientTest extends AbstractTestCase
{
    public function testCanCreateBranch()
    {
        $model = (new Branch())
            ->setBranchReference('foobar')
            ->setBranchName('foobar')
            ->setEmail('foo@bar.com')
            ->setLocation((new BranchLocation())
                ->setPropertyNumberOrName('foobar')
                ->setTownOrCity('foobar')
                ->setPostalCode('94112')
                ->setCountryCode('US')
            );

        $branch = $this->client->branch($model);
        $this->assertEquals('foobar', $branch->getBranchReference());
    }

    public function testCanCreateListing()
    {
        $model = (new Update())
            ->setBranchReference('foobar')
            ->setCategory(Update::CATEGORY_COMMERCIAL)
            ->setDetailedDescription([
                (new UpdateDetailedDescriptionItem())
                    ->setHeading('bar')
                    ->setText('baz'),
            ])
            ->setLifeCycleStatus(Update::LIFE_CYCLE_STATUS_LET_AGREED)
            ->setListingReference('foo-bar-baz')
            ->setLocation((new UpdateLocation())
                ->setCounty('US')
                ->setCountryCode('US')
                ->setTownOrCity('foobar')
                ->setStreetName('baz')
                ->setPropertyNumberOrName('foo')
            )
            ->setPricing((new UpdatePricing())
                ->setPrice(3000)
                ->setTransactionType(UpdatePricing::TRANSACTION_TYPE_RENT)
                ->setCurrencyCode('USD')
                ->setRentFrequency('per_week')
            )
            ->setPropertyType(Update::PROPERTY_TYPE_MAISONETTE);

        $property = $this->client->create($model);
        $this->assertEquals('foo-bar-baz', $property->getListingReference());
    }
}
