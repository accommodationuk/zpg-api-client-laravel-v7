<?php

return [
    'cert'     => [
        config_path('config/cert.crt'),
        ''
    ],
    'ssl_key' => config_path('config/private.pem'),
    'headers'  => [
        'Content-Type' => 'application/json',
        'User-Agent'   => 'Agent/0.0.1',
    ],
];
