diff --git a/Generator/Model/ClassGenerator.php b/Generator/Model/ClassGenerator.php
index fdb50e9..d77fee1 100644
--- a/Generator/Model/ClassGenerator.php
+++ b/Generator/Model/ClassGenerator.php
@@ -18,10 +18,11 @@ trait ClassGenerator
     /**
      * Return a model class.
      *
+     * @param Node[] $constants
      * @param Node[] $properties
      * @param Node[] $methods
      */
-    protected function createModel(string $name, array $properties, array $methods, bool $hasExtensions = false, bool $deprecated = false): Stmt\Class_
+    protected function createModel(string $name, array $constants, array $properties, array $methods, bool $hasExtensions = false, bool $deprecated = false): Stmt\Class_
     {
         $attributes = [];
 
@@ -38,7 +39,7 @@ EOD
         return new Stmt\Class_(
             $this->getNaming()->getClassName($name),
             [
-                'stmts' => array_merge($properties, $methods),
+                'stmts' => array_merge($constants, $properties, $methods),
                 'extends' => $hasExtensions ? new Name('\ArrayObject') : null,
             ],
             $attributes
diff --git a/Generator/ModelGenerator.php b/Generator/ModelGenerator.php
index 51967b2..a91b1e4 100644
--- a/Generator/ModelGenerator.php
+++ b/Generator/ModelGenerator.php
@@ -6,12 +6,15 @@ use Jane\JsonSchema\Generator\Context\Context;
 use Jane\JsonSchema\Generator\Model\ClassGenerator;
 use Jane\JsonSchema\Generator\Model\GetterSetterGenerator;
 use Jane\JsonSchema\Generator\Model\PropertyGenerator;
+use Jane\JsonSchema\Model\JsonSchema;
 use Jane\JsonSchema\Guesser\Guess\ClassGuess;
 use Jane\JsonSchema\Guesser\Guess\Property;
 use Jane\JsonSchema\Registry\Schema;
 use PhpParser\Node\Name;
 use PhpParser\Node\Stmt;
 use PhpParser\Parser;
+use PhpParser\Node\Const_;
+use PhpParser\Node\Scalar\String_;
 
 class ModelGenerator implements GeneratorInterface
 {
@@ -70,11 +73,32 @@ class ModelGenerator implements GeneratorInterface
 
             /** @var Property $property */
             foreach ($class->getProperties() as $property) {
+                /**
+                 * @var JsonSchema $object
+                 */
+                $object = $property->getObject();
+
+                if (method_exists($object, 'getEnum') && is_array($object->getEnum())) {
+                    $constants[] = $this->doCreateConstants($property, $object);
+                }
+
+                if (method_exists($object, 'getAnyOf') && is_array($object->getAnyOf())) {
+                    /**
+                     * @var JsonSchema[] $anyOf
+                     */
+                    $anyOfStmt = $object->getAnyOf();
+                    foreach ($anyOfStmt as $oneStmt) {
+                        if ($oneStmt->getType() == 'string' && $oneStmt->getEnum()) {
+                            $constants[] = $this->doCreateConstants($property, $oneStmt);
+                        }
+                    }
+                }
+
                 $properties[] = $this->createProperty($property, $namespace, null, $context->isStrict());
                 $methods = array_merge($methods, $this->doCreateClassMethods($class, $property, $namespace, $context->isStrict()));
             }
 
-            $model = $this->doCreateModel($class, $properties, $methods);
+            $model = $this->doCreateModel($class, $constants, $properties, $methods);
 
             $namespaceStmt = new Stmt\Namespace_(new Name($namespace), [$model]);
             $schema->addFile(new File($schema->getDirectory() . '/Model/' . $class->getName() . '.php', $namespaceStmt, self::FILE_TYPE_MODEL));
@@ -90,14 +114,28 @@ class ModelGenerator implements GeneratorInterface
         return $methods;
     }
 
-    protected function doCreateModel(ClassGuess $class, array $properties, array $methods): Stmt\Class_
+    protected function doCreateModel(ClassGuess $class, array $constants, array $properties, array $methods): Stmt\Class_
     {
         return $this->createModel(
             $class->getName(),
+            $constants,
             $properties,
             $methods,
             \count($class->getExtensionsType()) > 0,
             $class->isDeprecated()
         );
     }
+
+    protected function doCreateConstants(Property $property, $oneStmt)
+    {
+        $constants = [];
+        foreach ($oneStmt->getEnum() as $value) {
+            $constants[] = new Const_(
+                new Name(strtoupper(sprintf('%s_%s', $property->getName(), $value))),
+                new String_($value)
+            );
+        }
+
+        return new Stmt\ClassConst($constants, Stmt\Class_::MODIFIER_PUBLIC);
+    }
 }
