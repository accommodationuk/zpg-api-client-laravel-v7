<?php

namespace Zpg\Normalizer;

class NormalizerFactory
{
    public static function create()
    {
        $normalizers = array();
        $normalizers[] = new \Symfony\Component\Serializer\Normalizer\ArrayDenormalizer();
        $normalizers[] = new \Jane\JsonSchemaRuntime\Normalizer\ReferenceNormalizer();
        $normalizers[] = new AreaNormalizer();
        $normalizers[] = new BranchLocationNormalizer();
        $normalizers[] = new BranchLocationPafAddressNormalizer();
        $normalizers[] = new BranchNormalizer();
        $normalizers[] = new CoordinatesNormalizer();
        $normalizers[] = new DeleteNormalizer();
        $normalizers[] = new DimensionNormalizer();
        $normalizers[] = new GbNormalizer();
        $normalizers[] = new ListNormalizer();
        $normalizers[] = new MinMaxAreaNormalizer();
        $normalizers[] = new NormalizerFactory();
        $normalizers[] = new OverseasNormalizer();
        $normalizers[] = new UpdateAreasNormalizer();
        $normalizers[] = new UpdateContentItemNormalizer();
        $normalizers[] = new UpdateDetailedDescriptionItemNormalizer();
        $normalizers[] = new UpdateEpcRatingsNormalizer();
        $normalizers[] = new UpdateGoogleStreetViewNormalizer();
        $normalizers[] = new UpdateLeaseExpiryNormalizer();
        $normalizers[] = new UpdateLocationNormalizer();
        $normalizers[] = new UpdateLocationPafAddressNormalizer();
        $normalizers[] = new UpdateNormalizer();
        $normalizers[] = new UpdatePricingNormalizer();
        $normalizers[] = new UpdatePricingPricePerUnitAreaNormalizer();
        $normalizers[] = new UpdateServiceChargeNormalizer();
        $normalizers[] = new UpdateTenantEligibilityNormalizer();
        $normalizers[] = new ErrorResponseNormalizer();
        $normalizers[] = new ErrorResponseErrorsItemNormalizer();
        $normalizers[] = new BranchUpdateResponseNormalizer();
        $normalizers[] = new ListingUpdateResponseNormalizer();
        $normalizers[] = new ListingDeleteResponseNormalizer();
        $normalizers[] = new ListingListResponseNormalizer();
        $normalizers[] = new ListingListResponseListingsItemNormalizer();
        return $normalizers;
    }
}
