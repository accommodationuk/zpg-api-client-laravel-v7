<?php

namespace Zpg\Normalizer;

use Jane\JsonSchemaRuntime\Reference;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class CoordinatesNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type === 'Zpg\\Model\\Coordinates';
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof \Zpg\Model\Coordinates;
    }

    public function denormalize($data, $class, $format = null, array $context = array())
    {
        if (!is_object($data)) {
            return null;
        }
        if (isset($data->{'$ref'})) {
            return new Reference($data->{'$ref'}, $context['document-origin']);
        }
        $object = new \Zpg\Model\Coordinates();
        if (property_exists($data, 'latitude') && $data->{'latitude'} !== null) {
            $object->setLatitude($data->{'latitude'});
        }
        if (property_exists($data, 'longitude') && $data->{'longitude'} !== null) {
            $object->setLongitude($data->{'longitude'});
        }
        return $object;
    }

    public function normalize($object, $format = null, array $context = array())
    {
        $data = new \stdClass();
        if (null !== $object->getLatitude()) {
            $data->{'latitude'} = $object->getLatitude();
        }
        if (null !== $object->getLongitude()) {
            $data->{'longitude'} = $object->getLongitude();
        }
        return $data;
    }
}
