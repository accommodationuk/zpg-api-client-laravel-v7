<?php

namespace Zpg\Normalizer;

use Jane\JsonSchemaRuntime\Reference;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class UpdateNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type === 'Zpg\\Model\\Update';
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof \Zpg\Model\Update;
    }

    public function denormalize($data, $class, $format = null, array $context = array())
    {
        if (!is_object($data)) {
            return null;
        }
        if (isset($data->{'$ref'})) {
            return new Reference($data->{'$ref'}, $context['document-origin']);
        }
        $object = new \Zpg\Model\Update();
        if (property_exists($data, 'accessibility') && $data->{'accessibility'} !== null) {
            $object->setAccessibility($data->{'accessibility'});
        }
        if (property_exists($data, 'administration_fees') && $data->{'administration_fees'} !== null) {
            $object->setAdministrationFees($data->{'administration_fees'});
        }
        if (property_exists($data, 'annual_business_rates') && $data->{'annual_business_rates'} !== null) {
            $object->setAnnualBusinessRates($data->{'annual_business_rates'});
        }
        if (property_exists($data, 'areas') && $data->{'areas'} !== null) {
            $object->setAreas($this->denormalizer->denormalize($data->{'areas'}, 'Zpg\\Model\\UpdateAreas', 'json', $context));
        }
        if (property_exists($data, 'available_bedrooms') && $data->{'available_bedrooms'} !== null) {
            $object->setAvailableBedrooms($data->{'available_bedrooms'});
        }
        if (property_exists($data, 'available_from_date') && $data->{'available_from_date'} !== null) {
            $object->setAvailableFromDate($data->{'available_from_date'});
        }
        if (property_exists($data, 'basement') && $data->{'basement'} !== null) {
            $object->setBasement($data->{'basement'});
        }
        if (property_exists($data, 'bathrooms') && $data->{'bathrooms'} !== null) {
            $object->setBathrooms($data->{'bathrooms'});
        }
        if (property_exists($data, 'bills_included') && $data->{'bills_included'} !== null) {
            $values = array();
            foreach ($data->{'bills_included'} as $value) {
                $values[] = $value;
            }
            $object->setBillsIncluded($values);
        }
        if (property_exists($data, 'branch_reference') && $data->{'branch_reference'} !== null) {
            $object->setBranchReference($data->{'branch_reference'});
        }
        if (property_exists($data, 'burglar_alarm') && $data->{'burglar_alarm'} !== null) {
            $object->setBurglarAlarm($data->{'burglar_alarm'});
        }
        if (property_exists($data, 'business_for_sale') && $data->{'business_for_sale'} !== null) {
            $object->setBusinessForSale($data->{'business_for_sale'});
        }
        if (property_exists($data, 'buyer_incentives') && $data->{'buyer_incentives'} !== null) {
            $values_1 = array();
            foreach ($data->{'buyer_incentives'} as $value_1) {
                $values_1[] = $value_1;
            }
            $object->setBuyerIncentives($values_1);
        }
        if (property_exists($data, 'category') && $data->{'category'} !== null) {
            $object->setCategory($data->{'category'});
        }
        if (property_exists($data, 'central_heating') && $data->{'central_heating'} !== null) {
            $object->setCentralHeating($data->{'central_heating'});
        }
        if (property_exists($data, 'chain_free') && $data->{'chain_free'} !== null) {
            $object->setChainFree($data->{'chain_free'});
        }
        if (property_exists($data, 'commercial_use_classes') && $data->{'commercial_use_classes'} !== null) {
            $values_2 = array();
            foreach ($data->{'commercial_use_classes'} as $value_2) {
                $values_2[] = $value_2;
            }
            $object->setCommercialUseClasses($values_2);
        }
        if (property_exists($data, 'connected_utilities') && $data->{'connected_utilities'} !== null) {
            $values_3 = array();
            foreach ($data->{'connected_utilities'} as $value_3) {
                $values_3[] = $value_3;
            }
            $object->setConnectedUtilities($values_3);
        }
        if (property_exists($data, 'conservatory') && $data->{'conservatory'} !== null) {
            $object->setConservatory($data->{'conservatory'});
        }
        if (property_exists($data, 'construction_year') && $data->{'construction_year'} !== null) {
            $object->setConstructionYear($data->{'construction_year'});
        }
        if (property_exists($data, 'content') && $data->{'content'} !== null) {
            $values_4 = array();
            foreach ($data->{'content'} as $value_4) {
                $values_4[] = $this->denormalizer->denormalize($value_4, 'Zpg\\Model\\UpdateContentItem', 'json', $context);
            }
            $object->setContent($values_4);
        }
        if (property_exists($data, 'council_tax_band') && $data->{'council_tax_band'} !== null) {
            $object->setCouncilTaxBand($data->{'council_tax_band'});
        }
        if (property_exists($data, 'decorative_condition') && $data->{'decorative_condition'} !== null) {
            $object->setDecorativeCondition($data->{'decorative_condition'});
        }
        if (property_exists($data, 'deposit') && $data->{'deposit'} !== null) {
            $object->setDeposit($data->{'deposit'});
        }
        if (property_exists($data, 'detailed_description') && $data->{'detailed_description'} !== null) {
            $values_5 = array();
            foreach ($data->{'detailed_description'} as $value_5) {
                $values_5[] = $this->denormalizer->denormalize($value_5, 'Zpg\\Model\\UpdateDetailedDescriptionItem', 'json', $context);
            }
            $object->setDetailedDescription($values_5);
        }
        if (property_exists($data, 'display_address') && $data->{'display_address'} !== null) {
            $object->setDisplayAddress($data->{'display_address'});
        }
        if (property_exists($data, 'double_glazing') && $data->{'double_glazing'} !== null) {
            $object->setDoubleGlazing($data->{'double_glazing'});
        }
        if (property_exists($data, 'epc_ratings') && $data->{'epc_ratings'} !== null) {
            $object->setEpcRatings($this->denormalizer->denormalize($data->{'epc_ratings'}, 'Zpg\\Model\\UpdateEpcRatings', 'json', $context));
        }
        if (property_exists($data, 'feature_list') && $data->{'feature_list'} !== null) {
            $values_6 = array();
            foreach ($data->{'feature_list'} as $value_6) {
                $values_6[] = $value_6;
            }
            $object->setFeatureList($values_6);
        }
        if (property_exists($data, 'fireplace') && $data->{'fireplace'} !== null) {
            $object->setFireplace($data->{'fireplace'});
        }
        if (property_exists($data, 'fishing_rights') && $data->{'fishing_rights'} !== null) {
            $object->setFishingRights($data->{'fishing_rights'});
        }
        if (property_exists($data, 'floor_levels') && $data->{'floor_levels'} !== null) {
            $values_7 = array();
            foreach ($data->{'floor_levels'} as $value_7) {
                $value_8 = $value_7;
                if (is_int($value_7)) {
                    $value_8 = $value_7;
                } elseif (is_string($value_7)) {
                    $value_8 = $value_7;
                }
                $values_7[] = $value_8;
            }
            $object->setFloorLevels($values_7);
        }
        if (property_exists($data, 'floors') && $data->{'floors'} !== null) {
            $object->setFloors($data->{'floors'});
        }
        if (property_exists($data, 'furnished_state') && $data->{'furnished_state'} !== null) {
            $object->setFurnishedState($data->{'furnished_state'});
        }
        if (property_exists($data, 'google_street_view') && $data->{'google_street_view'} !== null) {
            $object->setGoogleStreetView($this->denormalizer->denormalize($data->{'google_street_view'}, 'Zpg\\Model\\UpdateGoogleStreetView', 'json', $context));
        }
        if (property_exists($data, 'ground_rent') && $data->{'ground_rent'} !== null) {
            $object->setGroundRent($data->{'ground_rent'});
        }
        if (property_exists($data, 'gym') && $data->{'gym'} !== null) {
            $object->setGym($data->{'gym'});
        }
        if (property_exists($data, 'lease_expiry') && $data->{'lease_expiry'} !== null) {
            $object->setLeaseExpiry($this->denormalizer->denormalize($data->{'lease_expiry'}, 'Zpg\\Model\\UpdateLeaseExpiry', 'json', $context));
        }
        if (property_exists($data, 'life_cycle_status') && $data->{'life_cycle_status'} !== null) {
            $object->setLifeCycleStatus($data->{'life_cycle_status'});
        }
        if (property_exists($data, 'listed_building_grade') && $data->{'listed_building_grade'} !== null) {
            $object->setListedBuildingGrade($data->{'listed_building_grade'});
        }
        if (property_exists($data, 'listing_reference') && $data->{'listing_reference'} !== null) {
            $object->setListingReference($data->{'listing_reference'});
        }
        if (property_exists($data, 'location') && $data->{'location'} !== null) {
            $object->setLocation($this->denormalizer->denormalize($data->{'location'}, 'Zpg\\Model\\UpdateLocation', 'json', $context));
        }
        if (property_exists($data, 'living_rooms') && $data->{'living_rooms'} !== null) {
            $object->setLivingRooms($data->{'living_rooms'});
        }
        if (property_exists($data, 'loft') && $data->{'loft'} !== null) {
            $object->setLoft($data->{'loft'});
        }
        if (property_exists($data, 'new_home') && $data->{'new_home'} !== null) {
            $object->setNewHome($data->{'new_home'});
        }
        if (property_exists($data, 'open_day') && $data->{'open_day'} !== null) {
            $object->setOpenDay($data->{'open_day'});
        }
        if (property_exists($data, 'outbuildings') && $data->{'outbuildings'} !== null) {
            $object->setOutbuildings($data->{'outbuildings'});
        }
        if (property_exists($data, 'outside_space') && $data->{'outside_space'} !== null) {
            $values_8 = array();
            foreach ($data->{'outside_space'} as $value_9) {
                $values_8[] = $value_9;
            }
            $object->setOutsideSpace($values_8);
        }
        if (property_exists($data, 'parking') && $data->{'parking'} !== null) {
            $values_9 = array();
            foreach ($data->{'parking'} as $value_10) {
                $values_9[] = $value_10;
            }
            $object->setParking($values_9);
        }
        if (property_exists($data, 'pets_allowed') && $data->{'pets_allowed'} !== null) {
            $object->setPetsAllowed($data->{'pets_allowed'});
        }
        if (property_exists($data, 'porter_security') && $data->{'porter_security'} !== null) {
            $object->setPorterSecurity($data->{'porter_security'});
        }
        if (property_exists($data, 'pricing') && $data->{'pricing'} !== null) {
            $object->setPricing($this->denormalizer->denormalize($data->{'pricing'}, 'Zpg\\Model\\UpdatePricing', 'json', $context));
        }
        if (property_exists($data, 'property_type') && $data->{'property_type'} !== null) {
            $value_11 = $data->{'property_type'};
            if (is_string($data->{'property_type'})) {
                $value_11 = $data->{'property_type'};
            } elseif (is_string($data->{'property_type'})) {
                $value_11 = $data->{'property_type'};
            }
            $object->setPropertyType($value_11);
        }
        if (property_exists($data, 'rateable_value') && $data->{'rateable_value'} !== null) {
            $object->setRateableValue($data->{'rateable_value'});
        }
        if (property_exists($data, 'rental_term') && $data->{'rental_term'} !== null) {
            $value_12 = $data->{'rental_term'};
            if (is_object($data->{'rental_term'})) {
                $value_12 = $data->{'rental_term'};
            } elseif (is_string($data->{'rental_term'})) {
                $value_12 = $data->{'rental_term'};
            }
            $object->setRentalTerm($value_12);
        }
        if (property_exists($data, 'repossession') && $data->{'repossession'} !== null) {
            $object->setRepossession($data->{'repossession'});
        }
        if (property_exists($data, 'retirement') && $data->{'retirement'} !== null) {
            $object->setRetirement($data->{'retirement'});
        }
        if (property_exists($data, 'sap_rating') && $data->{'sap_rating'} !== null) {
            $object->setSapRating($data->{'sap_rating'});
        }
        if (property_exists($data, 'service_charge') && $data->{'service_charge'} !== null) {
            $object->setServiceCharge($this->denormalizer->denormalize($data->{'service_charge'}, 'Zpg\\Model\\UpdateServiceCharge', 'json', $context));
        }
        if (property_exists($data, 'serviced') && $data->{'serviced'} !== null) {
            $object->setServiced($data->{'serviced'});
        }
        if (property_exists($data, 'shared_accommodation') && $data->{'shared_accommodation'} !== null) {
            $object->setSharedAccommodation($data->{'shared_accommodation'});
        }
        if (property_exists($data, 'summary_description') && $data->{'summary_description'} !== null) {
            $object->setSummaryDescription($data->{'summary_description'});
        }
        if (property_exists($data, 'swimming_pool') && $data->{'swimming_pool'} !== null) {
            $object->setSwimmingPool($data->{'swimming_pool'});
        }
        if (property_exists($data, 'tenanted') && $data->{'tenanted'} !== null) {
            $object->setTenanted($data->{'tenanted'});
        }
        if (property_exists($data, 'tenant_eligibility') && $data->{'tenant_eligibility'} !== null) {
            $object->setTenantEligibility($this->denormalizer->denormalize($data->{'tenant_eligibility'}, 'Zpg\\Model\\UpdateTenantEligibility', 'json', $context));
        }
        if (property_exists($data, 'tennis_court') && $data->{'tennis_court'} !== null) {
            $object->setTennisCourt($data->{'tennis_court'});
        }
        if (property_exists($data, 'tenure') && $data->{'tenure'} !== null) {
            $object->setTenure($data->{'tenure'});
        }
        if (property_exists($data, 'total_bedrooms') && $data->{'total_bedrooms'} !== null) {
            $object->setTotalBedrooms($data->{'total_bedrooms'});
        }
        if (property_exists($data, 'utility_room') && $data->{'utility_room'} !== null) {
            $object->setUtilityRoom($data->{'utility_room'});
        }
        if (property_exists($data, 'waterfront') && $data->{'waterfront'} !== null) {
            $object->setWaterfront($data->{'waterfront'});
        }
        if (property_exists($data, 'wood_floors') && $data->{'wood_floors'} !== null) {
            $object->setWoodFloors($data->{'wood_floors'});
        }
        return $object;
    }

    public function normalize($object, $format = null, array $context = array())
    {
        $data = new \stdClass();
        if (null !== $object->getAccessibility()) {
            $data->{'accessibility'} = $object->getAccessibility();
        }
        if (null !== $object->getAdministrationFees()) {
            $data->{'administration_fees'} = $object->getAdministrationFees();
        }
        if (null !== $object->getAnnualBusinessRates()) {
            $data->{'annual_business_rates'} = $object->getAnnualBusinessRates();
        }
        if (null !== $object->getAreas()) {
            $data->{'areas'} = $this->normalizer->normalize($object->getAreas(), 'json', $context);
        }
        if (null !== $object->getAvailableBedrooms()) {
            $data->{'available_bedrooms'} = $object->getAvailableBedrooms();
        }
        if (null !== $object->getAvailableFromDate()) {
            $data->{'available_from_date'} = $object->getAvailableFromDate();
        }
        if (null !== $object->getBasement()) {
            $data->{'basement'} = $object->getBasement();
        }
        if (null !== $object->getBathrooms()) {
            $data->{'bathrooms'} = $object->getBathrooms();
        }
        if (null !== $object->getBillsIncluded()) {
            $values = array();
            foreach ($object->getBillsIncluded() as $value) {
                $values[] = $value;
            }
            $data->{'bills_included'} = $values;
        }
        if (null !== $object->getBranchReference()) {
            $data->{'branch_reference'} = $object->getBranchReference();
        }
        if (null !== $object->getBurglarAlarm()) {
            $data->{'burglar_alarm'} = $object->getBurglarAlarm();
        }
        if (null !== $object->getBusinessForSale()) {
            $data->{'business_for_sale'} = $object->getBusinessForSale();
        }
        if (null !== $object->getBuyerIncentives()) {
            $values_1 = array();
            foreach ($object->getBuyerIncentives() as $value_1) {
                $values_1[] = $value_1;
            }
            $data->{'buyer_incentives'} = $values_1;
        }
        if (null !== $object->getCategory()) {
            $data->{'category'} = $object->getCategory();
        }
        if (null !== $object->getCentralHeating()) {
            $data->{'central_heating'} = $object->getCentralHeating();
        }
        if (null !== $object->getChainFree()) {
            $data->{'chain_free'} = $object->getChainFree();
        }
        if (null !== $object->getCommercialUseClasses()) {
            $values_2 = array();
            foreach ($object->getCommercialUseClasses() as $value_2) {
                $values_2[] = $value_2;
            }
            $data->{'commercial_use_classes'} = $values_2;
        }
        if (null !== $object->getConnectedUtilities()) {
            $values_3 = array();
            foreach ($object->getConnectedUtilities() as $value_3) {
                $values_3[] = $value_3;
            }
            $data->{'connected_utilities'} = $values_3;
        }
        if (null !== $object->getConservatory()) {
            $data->{'conservatory'} = $object->getConservatory();
        }
        if (null !== $object->getConstructionYear()) {
            $data->{'construction_year'} = $object->getConstructionYear();
        }
        if (null !== $object->getContent()) {
            $values_4 = array();
            foreach ($object->getContent() as $value_4) {
                $values_4[] = $this->normalizer->normalize($value_4, 'json', $context);
            }
            $data->{'content'} = $values_4;
        }
        if (null !== $object->getCouncilTaxBand()) {
            $data->{'council_tax_band'} = $object->getCouncilTaxBand();
        }
        if (null !== $object->getDecorativeCondition()) {
            $data->{'decorative_condition'} = $object->getDecorativeCondition();
        }
        if (null !== $object->getDeposit()) {
            $data->{'deposit'} = $object->getDeposit();
        }
        if (null !== $object->getDetailedDescription()) {
            $values_5 = array();
            foreach ($object->getDetailedDescription() as $value_5) {
                $values_5[] = $this->normalizer->normalize($value_5, 'json', $context);
            }
            $data->{'detailed_description'} = $values_5;
        }
        if (null !== $object->getDisplayAddress()) {
            $data->{'display_address'} = $object->getDisplayAddress();
        }
        if (null !== $object->getDoubleGlazing()) {
            $data->{'double_glazing'} = $object->getDoubleGlazing();
        }
        if (null !== $object->getEpcRatings()) {
            $data->{'epc_ratings'} = $this->normalizer->normalize($object->getEpcRatings(), 'json', $context);
        }
        if (null !== $object->getFeatureList()) {
            $values_6 = array();
            foreach ($object->getFeatureList() as $value_6) {
                $values_6[] = $value_6;
            }
            $data->{'feature_list'} = $values_6;
        }
        if (null !== $object->getFireplace()) {
            $data->{'fireplace'} = $object->getFireplace();
        }
        if (null !== $object->getFishingRights()) {
            $data->{'fishing_rights'} = $object->getFishingRights();
        }
        if (null !== $object->getFloorLevels()) {
            $values_7 = array();
            foreach ($object->getFloorLevels() as $value_7) {
                $value_8 = $value_7;
                if (is_int($value_7)) {
                    $value_8 = $value_7;
                } elseif (is_string($value_7)) {
                    $value_8 = $value_7;
                }
                $values_7[] = $value_8;
            }
            $data->{'floor_levels'} = $values_7;
        }
        if (null !== $object->getFloors()) {
            $data->{'floors'} = $object->getFloors();
        }
        if (null !== $object->getFurnishedState()) {
            $data->{'furnished_state'} = $object->getFurnishedState();
        }
        if (null !== $object->getGoogleStreetView()) {
            $data->{'google_street_view'} = $this->normalizer->normalize($object->getGoogleStreetView(), 'json', $context);
        }
        if (null !== $object->getGroundRent()) {
            $data->{'ground_rent'} = $object->getGroundRent();
        }
        if (null !== $object->getGym()) {
            $data->{'gym'} = $object->getGym();
        }
        if (null !== $object->getLeaseExpiry()) {
            $data->{'lease_expiry'} = $this->normalizer->normalize($object->getLeaseExpiry(), 'json', $context);
        }
        if (null !== $object->getLifeCycleStatus()) {
            $data->{'life_cycle_status'} = $object->getLifeCycleStatus();
        }
        if (null !== $object->getListedBuildingGrade()) {
            $data->{'listed_building_grade'} = $object->getListedBuildingGrade();
        }
        if (null !== $object->getListingReference()) {
            $data->{'listing_reference'} = $object->getListingReference();
        }
        if (null !== $object->getLocation()) {
            $data->{'location'} = $this->normalizer->normalize($object->getLocation(), 'json', $context);
        }
        if (null !== $object->getLivingRooms()) {
            $data->{'living_rooms'} = $object->getLivingRooms();
        }
        if (null !== $object->getLoft()) {
            $data->{'loft'} = $object->getLoft();
        }
        if (null !== $object->getNewHome()) {
            $data->{'new_home'} = $object->getNewHome();
        }
        if (null !== $object->getOpenDay()) {
            $data->{'open_day'} = $object->getOpenDay();
        }
        if (null !== $object->getOutbuildings()) {
            $data->{'outbuildings'} = $object->getOutbuildings();
        }
        if (null !== $object->getOutsideSpace()) {
            $values_8 = array();
            foreach ($object->getOutsideSpace() as $value_9) {
                $values_8[] = $value_9;
            }
            $data->{'outside_space'} = $values_8;
        }
        if (null !== $object->getParking()) {
            $values_9 = array();
            foreach ($object->getParking() as $value_10) {
                $values_9[] = $value_10;
            }
            $data->{'parking'} = $values_9;
        }
        if (null !== $object->getPetsAllowed()) {
            $data->{'pets_allowed'} = $object->getPetsAllowed();
        }
        if (null !== $object->getPorterSecurity()) {
            $data->{'porter_security'} = $object->getPorterSecurity();
        }
        if (null !== $object->getPricing()) {
            $data->{'pricing'} = $this->normalizer->normalize($object->getPricing(), 'json', $context);
        }
        if (null !== $object->getPropertyType()) {
            $value_11 = $object->getPropertyType();
            if (is_string($object->getPropertyType())) {
                $value_11 = $object->getPropertyType();
            } elseif (is_string($object->getPropertyType())) {
                $value_11 = $object->getPropertyType();
            }
            $data->{'property_type'} = $value_11;
        }
        if (null !== $object->getRateableValue()) {
            $data->{'rateable_value'} = $object->getRateableValue();
        }
        if (null !== $object->getRentalTerm()) {
            $value_12 = $object->getRentalTerm();
            if (is_object($object->getRentalTerm())) {
                $value_12 = $object->getRentalTerm();
            } elseif (is_string($object->getRentalTerm())) {
                $value_12 = $object->getRentalTerm();
            }
            $data->{'rental_term'} = $value_12;
        }
        if (null !== $object->getRepossession()) {
            $data->{'repossession'} = $object->getRepossession();
        }
        if (null !== $object->getRetirement()) {
            $data->{'retirement'} = $object->getRetirement();
        }
        if (null !== $object->getSapRating()) {
            $data->{'sap_rating'} = $object->getSapRating();
        }
        if (null !== $object->getServiceCharge()) {
            $data->{'service_charge'} = $this->normalizer->normalize($object->getServiceCharge(), 'json', $context);
        }
        if (null !== $object->getServiced()) {
            $data->{'serviced'} = $object->getServiced();
        }
        if (null !== $object->getSharedAccommodation()) {
            $data->{'shared_accommodation'} = $object->getSharedAccommodation();
        }
        if (null !== $object->getSummaryDescription()) {
            $data->{'summary_description'} = $object->getSummaryDescription();
        }
        if (null !== $object->getSwimmingPool()) {
            $data->{'swimming_pool'} = $object->getSwimmingPool();
        }
        if (null !== $object->getTenanted()) {
            $data->{'tenanted'} = $object->getTenanted();
        }
        if (null !== $object->getTenantEligibility()) {
            $data->{'tenant_eligibility'} = $this->normalizer->normalize($object->getTenantEligibility(), 'json', $context);
        }
        if (null !== $object->getTennisCourt()) {
            $data->{'tennis_court'} = $object->getTennisCourt();
        }
        if (null !== $object->getTenure()) {
            $data->{'tenure'} = $object->getTenure();
        }
        if (null !== $object->getTotalBedrooms()) {
            $data->{'total_bedrooms'} = $object->getTotalBedrooms();
        }
        if (null !== $object->getUtilityRoom()) {
            $data->{'utility_room'} = $object->getUtilityRoom();
        }
        if (null !== $object->getWaterfront()) {
            $data->{'waterfront'} = $object->getWaterfront();
        }
        if (null !== $object->getWoodFloors()) {
            $data->{'wood_floors'} = $object->getWoodFloors();
        }
        return $data;
    }
}
