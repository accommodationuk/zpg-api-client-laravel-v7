<?php

namespace Zpg\Model;

class UpdateDetailedDescriptionItem
{
    /**
     *
     *
     * @var Dimension|string
     */
    protected $dimensions;
    /**
     *
     *
     * @var string
     */
    protected $heading;
    /**
     *
     *
     * @var string
     */
    protected $text;

    /**
     *
     *
     * @return Dimension|string|null
     */
    public function getDimensions()
    {
        return $this->dimensions;
    }

    /**
     *
     *
     * @param Dimension|string|null $dimensions
     *
     * @return self
     */
    public function setDimensions($dimensions): self
    {
        $this->dimensions = $dimensions;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getHeading(): ?string
    {
        return $this->heading;
    }

    /**
     *
     *
     * @param string|null $heading
     *
     * @return self
     */
    public function setHeading(?string $heading): self
    {
        $this->heading = $heading;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     *
     *
     * @param string|null $text
     *
     * @return self
     */
    public function setText(?string $text): self
    {
        $this->text = $text;
        return $this;
    }
}
