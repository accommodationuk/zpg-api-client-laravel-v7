<?php

namespace Zpg\Model;

class ListingListResponse
{
    /**
     *
     *
     * @var string
     */
    protected $status;
    /**
     *
     *
     * @var string
     */
    protected $branchReference;
    /**
     *
     *
     * @var ListingListResponseListingsItem[]
     */
    protected $listings;

    /**
     *
     *
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     *
     *
     * @param string|null $status
     *
     * @return self
     */
    public function setStatus(?string $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getBranchReference(): ?string
    {
        return $this->branchReference;
    }

    /**
     *
     *
     * @param string|null $branchReference
     *
     * @return self
     */
    public function setBranchReference(?string $branchReference): self
    {
        $this->branchReference = $branchReference;
        return $this;
    }

    /**
     *
     *
     * @return ListingListResponseListingsItem[]|null
     */
    public function getListings(): ?array
    {
        return $this->listings;
    }

    /**
     *
     *
     * @param ListingListResponseListingsItem[]|null $listings
     *
     * @return self
     */
    public function setListings(?array $listings): self
    {
        $this->listings = $listings;
        return $this;
    }
}
