<?php

namespace Zpg\Model;

class Delete
{
    public const DELETION_REASON_WITHDRAWN = 'withdrawn', DELETION_REASON_OFFER_ACCEPTED = 'offer_accepted', DELETION_REASON_EXCHANGED = 'exchanged', DELETION_REASON_COMPLETED = 'completed', DELETION_REASON_LET = 'let';
    /**
     *
     *
     * @var string
     */
    protected $listingReference;
    /**
     *
     *
     * @var mixed
     */
    protected $deletionReason;

    /**
     *
     *
     * @return string|null
     */
    public function getListingReference(): ?string
    {
        return $this->listingReference;
    }

    /**
     *
     *
     * @param string|null $listingReference
     *
     * @return self
     */
    public function setListingReference(?string $listingReference): self
    {
        $this->listingReference = $listingReference;
        return $this;
    }

    /**
     *
     *
     * @return mixed
     */
    public function getDeletionReason()
    {
        return $this->deletionReason;
    }

    /**
     *
     *
     * @param mixed $deletionReason
     *
     * @return self
     */
    public function setDeletionReason($deletionReason): self
    {
        $this->deletionReason = $deletionReason;
        return $this;
    }
}
