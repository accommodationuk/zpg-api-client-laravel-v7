<?php

namespace Zpg\Model;

class ListingListResponseListingsItem
{
    /**
     *
     *
     * @var string
     */
    protected $listingReference;
    /**
     *
     *
     * @var string
     */
    protected $listingEtag;
    /**
     *
     *
     * @var string
     */
    protected $url;

    /**
     *
     *
     * @return string|null
     */
    public function getListingReference(): ?string
    {
        return $this->listingReference;
    }

    /**
     *
     *
     * @param string|null $listingReference
     *
     * @return self
     */
    public function setListingReference(?string $listingReference): self
    {
        $this->listingReference = $listingReference;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getListingEtag(): ?string
    {
        return $this->listingEtag;
    }

    /**
     *
     *
     * @param string|null $listingEtag
     *
     * @return self
     */
    public function setListingEtag(?string $listingEtag): self
    {
        $this->listingEtag = $listingEtag;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     *
     *
     * @param string|null $url
     *
     * @return self
     */
    public function setUrl(?string $url): self
    {
        $this->url = $url;
        return $this;
    }
}
