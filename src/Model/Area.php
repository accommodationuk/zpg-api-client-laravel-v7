<?php

namespace Zpg\Model;

class Area
{
    /**
     *
     *
     * @var mixed
     */
    protected $units;
    /**
     *
     *
     * @var float
     */
    protected $value;

    /**
     *
     *
     * @return mixed
     */
    public function getUnits()
    {
        return $this->units;
    }

    /**
     *
     *
     * @param mixed $units
     *
     * @return self
     */
    public function setUnits($units): self
    {
        $this->units = $units;
        return $this;
    }

    /**
     *
     *
     * @return float|null
     */
    public function getValue(): ?float
    {
        return $this->value;
    }

    /**
     *
     *
     * @param float|null $value
     *
     * @return self
     */
    public function setValue(?float $value): self
    {
        $this->value = $value;
        return $this;
    }
}
