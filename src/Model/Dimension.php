<?php

namespace Zpg\Model;

class Dimension
{
    public const UNITS_FEET = 'feet', UNITS_METRES = 'metres';
    /**
     *
     *
     * @var float
     */
    protected $length;
    /**
     *
     *
     * @var float
     */
    protected $width;
    /**
     *
     *
     * @var mixed
     */
    protected $units;

    /**
     *
     *
     * @return float|null
     */
    public function getLength(): ?float
    {
        return $this->length;
    }

    /**
     *
     *
     * @param float|null $length
     *
     * @return self
     */
    public function setLength(?float $length): self
    {
        $this->length = $length;
        return $this;
    }

    /**
     *
     *
     * @return float|null
     */
    public function getWidth(): ?float
    {
        return $this->width;
    }

    /**
     *
     *
     * @param float|null $width
     *
     * @return self
     */
    public function setWidth(?float $width): self
    {
        $this->width = $width;
        return $this;
    }

    /**
     *
     *
     * @return mixed
     */
    public function getUnits()
    {
        return $this->units;
    }

    /**
     *
     *
     * @param mixed $units
     *
     * @return self
     */
    public function setUnits($units): self
    {
        $this->units = $units;
        return $this;
    }
}
