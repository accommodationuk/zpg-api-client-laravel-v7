<?php

namespace Zpg\Model;

class UpdateLeaseExpiry
{
    /**
     *
     *
     * @var string
     */
    protected $expiryDate;
    /**
     *
     *
     * @var int
     */
    protected $yearsRemaining;

    /**
     *
     *
     * @return string|null
     */
    public function getExpiryDate(): ?string
    {
        return $this->expiryDate;
    }

    /**
     *
     *
     * @param string|null $expiryDate
     *
     * @return self
     */
    public function setExpiryDate(?string $expiryDate): self
    {
        $this->expiryDate = $expiryDate;
        return $this;
    }

    /**
     *
     *
     * @return int|null
     */
    public function getYearsRemaining(): ?int
    {
        return $this->yearsRemaining;
    }

    /**
     *
     *
     * @param int|null $yearsRemaining
     *
     * @return self
     */
    public function setYearsRemaining(?int $yearsRemaining): self
    {
        $this->yearsRemaining = $yearsRemaining;
        return $this;
    }
}
