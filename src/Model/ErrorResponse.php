<?php

namespace Zpg\Model;

class ErrorResponse
{
    /**
     *
     *
     * @var string
     */
    protected $errorName;
    /**
     *
     *
     * @var string
     */
    protected $errorAdvice;
    /**
     *
     *
     * @var ErrorResponseErrorsItem[]
     */
    protected $errors;
    /**
     *
     *
     * @var string
     */
    protected $schema;
    /**
     *
     *
     * @var string
     */
    protected $status;

    /**
     *
     *
     * @return string|null
     */
    public function getErrorName(): ?string
    {
        return $this->errorName;
    }

    /**
     *
     *
     * @param string|null $errorName
     *
     * @return self
     */
    public function setErrorName(?string $errorName): self
    {
        $this->errorName = $errorName;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getErrorAdvice(): ?string
    {
        return $this->errorAdvice;
    }

    /**
     *
     *
     * @param string|null $errorAdvice
     *
     * @return self
     */
    public function setErrorAdvice(?string $errorAdvice): self
    {
        $this->errorAdvice = $errorAdvice;
        return $this;
    }

    /**
     *
     *
     * @return ErrorResponseErrorsItem[]|null
     */
    public function getErrors(): ?array
    {
        return $this->errors;
    }

    /**
     *
     *
     * @param ErrorResponseErrorsItem[]|null $errors
     *
     * @return self
     */
    public function setErrors(?array $errors): self
    {
        $this->errors = $errors;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getSchema(): ?string
    {
        return $this->schema;
    }

    /**
     *
     *
     * @param string|null $schema
     *
     * @return self
     */
    public function setSchema(?string $schema): self
    {
        $this->schema = $schema;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     *
     *
     * @param string|null $status
     *
     * @return self
     */
    public function setStatus(?string $status): self
    {
        $this->status = $status;
        return $this;
    }
}
