<?php

namespace Zpg\Model;

class BranchUpdateResponse
{
    /**
     *
     *
     * @var string
     */
    protected $status;
    /**
     *
     *
     * @var string
     */
    protected $branchReference;
    /**
     *
     *
     * @var bool
     */
    protected $newBranch;

    /**
     *
     *
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     *
     *
     * @param string|null $status
     *
     * @return self
     */
    public function setStatus(?string $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getBranchReference(): ?string
    {
        return $this->branchReference;
    }

    /**
     *
     *
     * @param string|null $branchReference
     *
     * @return self
     */
    public function setBranchReference(?string $branchReference): self
    {
        $this->branchReference = $branchReference;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getNewBranch(): ?bool
    {
        return $this->newBranch;
    }

    /**
     *
     *
     * @param bool|null $newBranch
     *
     * @return self
     */
    public function setNewBranch(?bool $newBranch): self
    {
        $this->newBranch = $newBranch;
        return $this;
    }
}
