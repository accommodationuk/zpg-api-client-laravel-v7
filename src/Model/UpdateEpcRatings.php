<?php

namespace Zpg\Model;

class UpdateEpcRatings
{
    /**
     *
     * @var int|null
     */
    protected ?int $eerCurrentRating;
    /**
     *
     *
     * @var int|null
     */
    protected ?int $eerPotentialRating;
    /**
     *
     *
     * @var int|null
     */
    protected ?int $eirCurrentRating;
    /**
     *
     *
     * @var int|null
     */
    protected ?int $eirPotentialRating;

    /**
     * Setter function for EER Current Rating.
     *
     * @deprecated
     * @note This is not necessary for an agent to supply as of new legislation in 2020.
     *       GOV.UK can be used for the EPC/EIR information that is most up to date.
     *       Agents do not need to send this.
     * @see [ACUK-3478]
     *
     * @return int
     */
    public function getEerCurrentRating(): int
    {
        return $this->eerCurrentRating ?? 1;
    }

    /**
     * Setter function for EER Current Rating.
     *
     * @deprecated
     * @note This is not necessary for an agent to supply as of new legislation in 2020.
     *       GOV.UK can be used for the EPC/EIR information that is most up to date.
     *       Agents do not need to send this.
     * @see [ACUK-3478]
     *
     * @param int|null $eerCurrentRating
     *
     * @return self
     */
    public function setEerCurrentRating(?int $eerCurrentRating): self
    {
        $this->eerCurrentRating = $eerCurrentRating;
        return $this;
    }

    /**
     * Setter function for EER Potential Rating.
     *
     * @deprecated
     * @note This is not necessary for an agent to supply as of new legislation in 2020.
     *       GOV.UK can be used for the EPC/EIR information that is most up to date.
     *       Agents do not need to send this.
     * @see [ACUK-3478]
     *
     * @return int
     */
    public function getEerPotentialRating(): int
    {
        return $this->eerPotentialRating ?? 1;
    }

    /**
     * Setter function for EER Potential Rating.
     *
     * @deprecated
     * @note This is not necessary for an agent to supply as of new legislation in 2020.
     *       GOV.UK can be used for the EPC/EIR information that is most up to date.
     *       Agents do not need to send this.
     * @see [ACUK-3478]
     *
     * @param int|null $eerPotentialRating
     *
     * @return self
     */
    public function setEerPotentialRating(?int $eerPotentialRating): self
    {
        $this->eerPotentialRating = $eerPotentialRating;
        return $this;
    }

    /**
     * Setter function for EIR Current Rating.
     *
     * @deprecated
     * @note This is not necessary for an agent to supply as of new legislation in 2020.
     *       GOV.UK can be used for the EPC/EIR information that is most up to date.
     *       Agents do not need to send this.
     * @see [ACUK-3478]
     *
     * @return int
     */
    public function getEirCurrentRating(): int
    {
        return $this->eirCurrentRating ?? 1;
    }

    /**
     * Setter function for EIR Current Rating.
     *
     * @deprecated
     * @note This is not necessary for an agent to supply as of new legislation in 2020.
     *       GOV.UK can be used for the EPC/EIR information that is most up to date.
     *       Agents do not need to send this.
     * @see [ACUK-3478]
     *
     * @param int|null $eirCurrentRating
     *
     * @return self
     */
    public function setEirCurrentRating(?int $eirCurrentRating): self
    {
        $this->eirCurrentRating = $eirCurrentRating;
        return $this;
    }

    /**
     * Getter function for EIR Potential Rating.
     *
     * @deprecated
     * @note This is not necessary for an agent to supply as of new legislation in 2020.
     *       GOV.UK can be used for the EPC/EIR information that is most up to date.
     *       Agents do not need to send this.
     *
     * @see [ACUK-3478]
     *
     * @return int
     */
    public function getEirPotentialRating(): int
    {
        return $this->eirPotentialRating ?? 1;
    }

    /**
     * Setter function for EIR Potential Rating.
     *
     * @deprecated
     * @note This is not necessary for an agent to supply as of new legislation in 2020.
     *       GOV.UK can be used for the EPC/EIR information that is most up to date.
     *       Agents do not need to send this.
     *
     * @see [ACUK-3478]
     *
     * @param int|null $eirPotentialRating
     *
     * @return self
     */
    public function setEirPotentialRating(?int $eirPotentialRating): self
    {
        $this->eirPotentialRating = $eirPotentialRating;
        return $this;
    }
}
