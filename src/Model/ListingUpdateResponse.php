<?php

namespace Zpg\Model;

class ListingUpdateResponse
{
    /**
     *
     *
     * @var string
     */
    protected $status;
    /**
     *
     *
     * @var string
     */
    protected $listingReference;
    /**
     *
     *
     * @var string
     */
    protected $listingEtag;
    /**
     *
     *
     * @var string
     */
    protected $url;
    /**
     *
     *
     * @var bool
     */
    protected $newListing;

    /**
     *
     *
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     *
     *
     * @param string|null $status
     *
     * @return self
     */
    public function setStatus(?string $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getListingReference(): ?string
    {
        return $this->listingReference;
    }

    /**
     *
     *
     * @param string|null $listingReference
     *
     * @return self
     */
    public function setListingReference(?string $listingReference): self
    {
        $this->listingReference = $listingReference;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getListingEtag(): ?string
    {
        return $this->listingEtag;
    }

    /**
     *
     *
     * @param string|null $listingEtag
     *
     * @return self
     */
    public function setListingEtag(?string $listingEtag): self
    {
        $this->listingEtag = $listingEtag;
        return $this;
    }

    /**
     *
     *
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     *
     *
     * @param string|null $url
     *
     * @return self
     */
    public function setUrl(?string $url): self
    {
        $this->url = $url;
        return $this;
    }

    /**
     *
     *
     * @return bool|null
     */
    public function getNewListing(): ?bool
    {
        return $this->newListing;
    }

    /**
     *
     *
     * @param bool|null $newListing
     *
     * @return self
     */
    public function setNewListing(?bool $newListing): self
    {
        $this->newListing = $newListing;
        return $this;
    }
}
